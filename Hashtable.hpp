/*
 * @file: Hashtable.hpp
 * @author: Aaron Rawson
 */

#pragma once

#include "LinkedList.hpp"
#include <cmath>

template <typename T>
class Hashtable {
public:
    typedef unsigned long (*HashFn)(T);
    typedef bool (*EqualityPr)(T,T);

    Hashtable(int expectedItems, float desiredLoadFactor, HashFn hashFunction, EqualityPr predicate)
    : hashFunction(hashFunction), equal(predicate),
    buckets(std::max((int) ((float) expectedItems / desiredLoadFactor), 1)),
    data(new LinkedList<T>*[buckets]) {
        for (int i=0; i<buckets; i++) {
            data[i] = new LinkedList<T>(predicate);
        }
    }

    ~Hashtable() {
        delete[] data;
    }
    
    int get_entries() {
        int entries = 0;
        for (int i = 0; i < buckets; i++) {
            entries += data[i]->size();
        }
        return entries;
    }

    float get_load_factor() {
        float entries = get_entries();
        return entries/(float)buckets;
    }

    float get_load_deviation() {
        float mean = get_load_factor();
        float sum = 0;
        for (int i = 0; i < buckets; i++) {
            sum += std::pow(data[i]->size() - mean, 2);
        }
        return std::sqrt(sum / buckets);
    }

    void add(T t) {
        LinkedList<T> *cell = get_cell_for(t);
        if (!cell->contains(t)) {
            cell->add(t);
        }
    }

    bool contains(T t) {
        return get_cell_for(t)->contains(t);
    }
    
    T getEquivalent(T t) {
        return get_cell_for(t)->getEquivalent(t);
    }

private:
    const HashFn hashFunction;
    const EqualityPr equal;
    const int buckets;
    LinkedList<T>* *data;
    
    LinkedList<T>* get_cell_for(T t) {
        unsigned long hash = hashFunction(t);
        return data[hash % buckets];
    }
};
