/* 
 * File:   HashFns.hpp
 * Author: arawson
 *
 * Created on February 20, 2013, 12:04 PM
 */

#include "City.hpp"

#include <iostream>
using std::cout;
using std::endl;

#pragma once

// Daniel Bernstein's algorithm from comp.lang.c
unsigned long djb2(const char str[]) {
    unsigned long hash = 5381;
    int c = *str;
    
    do {
        hash = hash * 33 ^ c;
        
        ++str;
        c = *str;
    } while (c != 0);
    return hash;
}

unsigned long djb2(City c) {
    return djb2(c.get_name().c_str());
}

//djb2 resembles a linear congruential generator
//which is a type of psuedo random number generator that uses a
//seed sequence, here, I seed the generator with the string
unsigned long lcg1(const char str[]) {
    const unsigned long seed = 5101;
    const unsigned long modulus = 631;
    
    unsigned long hash = seed;
    int c = *str;
    do {
        hash = hash * modulus ^ c;
        
        ++str;
        c = *str;
    } while (c!=0);
    return hash;
}

unsigned long lcg1(City c) {
    return lcg1(c.get_name().c_str());
}

/*comparison of the hash funtions using long.txt
 * from project 1 as the test dataset and a load
 * factor of 0.50
 * 
 * name: standard deviation
 * djb2: 0.680054
  * lcg1(5101, 631): 0.66854
 * lcg1(29, 3): 0.79234
 * lcg1(999983, 900007): 0.801434
 */
