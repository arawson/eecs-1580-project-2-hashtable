cmake_minimum_required (VERSION 2.6)

project (HashTable)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

if (MSVC)
    set (CMAKE_CXX_FLAGS "" "${CMAKE_CXX_FLAGS}")
endif()
if (CMAKE_COMPILER_IS_GNUCXX)
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ansi -Wall -Werror -O0")
endif()

add_library (City City.cpp)

add_executable (Main main.cpp)

target_link_libraries (Main City)

