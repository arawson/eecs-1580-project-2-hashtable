/* 
 * File:   main.cpp
 * Author: Aaron Rawson
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include "City.hpp"
#include "LinkedList.hpp"
#include "Hashtable.hpp"
#include "HashFns.hpp"

using namespace std;

const Hashtable<City>::HashFn HASH_FUNCTION = &lcg1;
const float LOAD_FACTOR = 0.50;

Hashtable<City> *load_cities(ifstream &inFile);
void writeOut(string name, City cities[], int length);

int main(int argc, char** argv) {
    string input_name;
    ifstream inFile;
    cout << "Enter the name of the file to read: ";
    cin >> input_name;
    inFile.open(input_name.c_str(), ifstream::in);
    if (!inFile.good()) {
        cout << "could not open file: " << input_name << endl;
        return EXIT_FAILURE;
    }
    Hashtable<City> *table = load_cities(inFile);
    
    if (table == NULL) {
        cout << "Error loading table. Abort." << endl;
        return EXIT_FAILURE;
    }
    
    string response = "";
    cout << "Enter the name of a city to retrieve (or stop to quit): ";
    cin >> response;
    while (response != "stop") {
        City surrogate;
        surrogate.set_name(response);
        if (table->contains(surrogate)) {
            cout << table->getEquivalent(surrogate) << endl;
        } else {
            cout << "No city with the name " << surrogate.get_name() << " was found in the table." << endl;
        }
        
        cout << "Enter the name of a city to retrieve (or stop to quit): ";
        cin >> response;
    }
    
    delete table;
}

void writeOut(string name, City cities[], int length) {
    ofstream output(name.c_str(), ifstream::trunc);
    
    if (!output.good()) {
        cout << "Could not open file \"" << name << "\" for writing!" << endl;
        return;
    }
    
    for (int i = 0; i < length; i++) {
        output << cities[i] << endl;
    }
    output.close();
}

Hashtable<City> *load_cities(ifstream &inFile) {
    LinkedList<City> lines(*nameEquality);
    string line;
    for (; !inFile.eof(); getline(inFile, line)) {
        if (line.length() > 0) {
            lines.add(City(line));
        }
    }
    
    Hashtable<City> *result = NULL;
    result = new Hashtable<City>(lines.size(), LOAD_FACTOR, HASH_FUNCTION, &nameEquality);
    
    for (ListItem<City> *i = lines.get_head(); i->getNext() != NULL; i=i->getNext()) {
        result->add(i->getDatum());
    }
    
    cout << "Loaded " << lines.size() << " cities into the table." << endl;
    cout << "The hash table's average load factor is "
            << result-> get_load_factor() << " ." << endl;
    cout << "The hash table's load has a standard deviation of "
            << result->get_load_deviation() << " sigma." << endl;
    
    return result;
}
