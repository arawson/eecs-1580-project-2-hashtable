/*
 * @file: LinkedList.hpp
 * @author: Aaron Rawson
 */

#pragma once

#include <cstddef>

template <typename T>
class ListItem {
private:
    ListItem(T datum, ListItem<T> *next = NULL) : datum(datum), next(next) {}
    template <typename U>
        friend class LinkedList;

public:
    ~ListItem() {
        if (next != NULL) {
            delete next;
        }
    }
    ListItem *getNext() {return next;}
    T getDatum() {return datum;}

private:
    T datum;
    ListItem<T> *next;
};

template <typename T>
class LinkedList {
public:
    typedef bool (*EqualityPr)(T,T);
    
    LinkedList(EqualityPr predicate) : equal(predicate), head(NULL) {}
    ~LinkedList() {
        delete head;
    }
    
    ListItem<T>* find(T t) {
        if (head == NULL) {
            return NULL;
        }
        for (ListItem<T> *item = head; item != NULL; item = item->getNext()) {
            if (equal(item->getDatum(), t)) {
                return item;
            }
        }
        return NULL;
    }
    
    ListItem<T> *get_head() {
        return head;
    }

    ListItem<T> *get_tail() {
        ListItem<T> *r = NULL;
        for (ListItem<T> *item = head; item != NULL; item = item -> getNext()) {
            if (item->getNext() == NULL) {
                r = item;
                break;
            }
        }
        return r;
    }

    void add(T t) {
        ListItem<T> *tail = get_tail();
        if (tail == NULL) {
            head = new ListItem<T>(t);
        } else {
            tail->next = new ListItem<T>(t);
        }
    }

    void remove(T t) {
        ListItem<T> *before(NULL), *found(NULL), *after(NULL);
        for (ListItem<T> *i = head; i != NULL; i = i->getNext()) {
            if (equal(i->getDatum(),t)) {
                found = i;
                break;
            }
            before = i;
        }
        if (found == NULL) {
            return;
        }
        after = found->next;
        if (before == NULL) {
            head = after;
        } else {
            before->next = after;
        }
        found->next = NULL;
        delete found;
    }

    bool contains(T t) {
        return find(t) != NULL;
    }
    
    T getEquivalent(T t) {
        return find(t)->getDatum();
    }

    int size() {
        int count = 0;
        ListItem<T> *next = head;
        while (next != NULL) {
            next = next->next;
            count++;
        }
        return count;
    }

private:
    EqualityPr equal;
    ListItem<T> *head;
};

